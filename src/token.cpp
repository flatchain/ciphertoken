#include <token.hpp>
#include <secp256k1.h>
#include <secp256k1_elgamal.h>
#include <secp256k1_bulletproofs.h>
#include <string>
#include <eosio/crypto.hpp>
#include <eosio/print.hpp>

using namespace std;

int getHexVal(char c)
{
  if(c >= '0' && c<= '9')
    return c - '0';
  else if(c >= 'a' && c<= 'f')
    return c - 'a' + 10;
  else if(c >= 'A' && c<= 'F')
    return c - 'A' + 10;
  else
    return -1;//error
}

unsigned char* hexToAscii(const char* hexStr, int len, unsigned char * ascStr)
{
	for(int i = 0; i < len; i +=2)
	{
		ascStr[i/2] = (getHexVal(hexStr[i])*16 + getHexVal(hexStr[i+1]));
	}

	return ascStr;
}

ACTION token::transfer(name from, name to, /*symbol symbol, */string fpubkey, string tpubkey, string eq_para, string from_pub_para, string to_pub_para, string signature, string memo) {
	//require_auth(from);
	//check( is_account(to), "to account does not exists");

	//require_recipient( from );
	//require_recipient( to );
	int ret = 0;
	const secp256k1_context* ctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY);
	check(ctx != NULL, "secp256k1 create context failed");

	//string ca = from_acnts.get(symbol.code().raw(), "no balance object found");

	// string cb;
	// auto t = to_acnts.find(symbol.code().raw());
	// if (t != to_acnts.end()) {
	// 	cb = t->cipher;
	// }

	// 0. extract C from ca1, ca2, cb1
	//		0.1 use ca1.Y - ca2.Y - cb1.Y as public key, to validate signature
	secp256k1_ecdsa_signature sig;
	memset(&sig.data, 0, sizeof(sig.data));
	hexToAscii(signature.c_str(), 128, sig.data);

	unsigned char eq_para_sz[288] = { 0 };
	hexToAscii(eq_para.c_str(), 576, eq_para_sz);

	unsigned char from_pub_para_sz[995] = { 0 };
	hexToAscii(from_pub_para.c_str(), 1990, from_pub_para_sz);

	unsigned char to_pub_para_sz[995] = { 0 };
	hexToAscii(to_pub_para.c_str(), 1990, to_pub_para_sz);

	secp256k1_pubkey pubkey;
	memset(&pubkey, 0, sizeof(pubkey));
	secp256k1_elgamal_pubkey(ctx, &pubkey, &eq_para_sz[64], &from_pub_para_sz[64], &to_pub_para_sz[64]);

	// unsigned char msg[2278] = { 0 };
	// memcpy(msg, eq_para_sz, 288);
	// memcpy(&msg[288], from_pub_para_sz, 995);
	// memcpy(&msg[288 + 995], to_pub_para_sz, 995);

	secp256k1_sha256_ex hasher;
	unsigned char pa[33] = { 0 };
	secp256k1_sha256_initialize_ex(&hasher);
	secp256k1_sha256_write_ex(&hasher, eq_para_sz, 288);
	secp256k1_sha256_write_ex(&hasher, from_pub_para_sz, 995);
	secp256k1_sha256_write_ex(&hasher, to_pub_para_sz, 995);
	secp256k1_sha256_finalize_ex(&hasher, pa);

	//check(secp256k1_ecdsa_verify(ctx, &sig, sha256((const char *)msg, 2278).extract_as_byte_array().data(), &pubkey), "signature validation failed");
	check(secp256k1_ecdsa_verify(ctx, &sig, pa, &pubkey), "signature validation failed");

	//string ca1_bal, ca2_bal, cb1_bal;

	// 1. verify ca1 ≈ ca
	//		1.1 extract C, prove from ca1
	//		1.2 valid, prove valid

	//unsigned char pa[33] = { 0 };
	memset(pa, 0, 33);
	hexToAscii(fpubkey.c_str(), 66, pa);

	memset(&pubkey, 0, sizeof(pubkey));
	check(secp256k1_ec_pubkey_parse(ctx, &pubkey, pa, 33), "parse public key failed");

	accounts acnts(_self, _self.value);
	auto c = acnts.get("holyshitmans"_n.value, "no from accounts found");
	unsigned char balance_a[128] = { 0 };
	hexToAscii(c.cipher.c_str(), 256, balance_a);

	check(secp256k1_elgamal_equal_verify(ctx, &eq_para_sz[128], balance_a, eq_para_sz, &pubkey), "verify equal prove failed");

	// 2. verify ca2 use from's public key encrypted
	//		2.1 extract C, rangeproof, prove from ca2
	//		2.2 rangeproof valid, prove valid
	secp256k1_pedersen_commitment commit;
	secp256k1_bulletproof_string_to_commit(&commit, &from_pub_para_sz[64], 64);

	secp256k1_scratch_space *scratch = secp256k1_scratch_space_create(ctx, 20000);
    secp256k1_bulletproof_generators *generators = secp256k1_bulletproof_generators_create(ctx, &secp256k1_generator_const_g, 128);
	check(secp256k1_bulletproof_rangeproof_verify(ctx, scratch, generators, &from_pub_para_sz[128], 675, NULL, &commit, 1, 64, &secp256k1_generator_const_h, NULL, 0),
			"verify rangeproofs ca2 failed");

	check(secp256k1_elgamal_valid_verify(ctx, &from_pub_para_sz[128+675], from_pub_para_sz, &pubkey), "verify valid prove of ca2 failed");

	// 3. verify cb1 use to's public key encrypted
	//		3.1 extract C, rangeproof, prove from cb1
	//		3.2 rangeproof valid, prove valid
	memset(&commit, 0, sizeof(commit));
	secp256k1_bulletproof_string_to_commit(&commit, &to_pub_para_sz[64], 64);
	check(secp256k1_bulletproof_rangeproof_verify(ctx, scratch, generators, &to_pub_para_sz[128], 675, NULL, &commit, 1, 64, &secp256k1_generator_const_h, NULL, 0),
			"verify rangeproofs cb1 failed");

	memset(pa, 0, 33);
	hexToAscii(tpubkey.c_str(), 66, pa);
	memset(&pubkey, 0, sizeof(pubkey));
	check(secp256k1_ec_pubkey_parse(ctx, &pubkey, pa, 33), "parse public key failed");
	check(secp256k1_elgamal_valid_verify(ctx, &to_pub_para_sz[128+675], to_pub_para_sz, &pubkey), "verify valid prove of cb1 failed");

	// 4. update cipher balance
	//		4.1 set from's balance as ca2
	//		4.2 set to's balance as cb + cb1

	// Done!
}

ACTION token::foo()
{
	require_auth(_self);

	string c_a = "68e294b9520ccfdeffb6a2091c407beb477f5bd3cb411df422f220589c0ec57d7bca90af21f4e617a895be9a12d2254a776001be4063efb320ab539f3262883d3cb379eec4eb69ca08626b0ed3c0ffe237d37abd84141ebb4cf99d8f397032c96565b72e11a5f52d33c7a16b163b29f4819b24471deceaa2708498cd8997987c";
	accounts acnts(_self, _self.value);
	auto c = acnts.find("holyshitmans"_n.value);
	if (c == acnts.end()) {
		acnts.emplace(_self, [&](auto &a){
			a.id = "holyshitmans"_n;
			a.cipher = c_a;
		});
	} else {
		acnts.modify(*c, _self, [&](auto &a){
			a.cipher = c_a;
		});
	}

	string c_b = "8786d19e474007a79e9e8276ad66bec663071754bc091c75f4a7f39db887c7315059783c82b522e4664dd2c6a07e04d5c6720c56e65b954bd5bbe32a02c559cc2b2c905a4565e829ea178da7c173fa70f8452e13c2d329ee46c1753a3dcde54cd8823507682bd0366bd2af08ea7506c2537cd6392315a7f773314e957c2dda1a";
	c = acnts.find("ciphertext11"_n.value);
	if (c == acnts.end()) {
		acnts.emplace(_self, [&](auto &a){
			a.id = "ciphertext11"_n;
			a.cipher = c_b;
		});
	} else {
		acnts.modify(*c, _self, [&](auto &a){
			a.cipher = c_b;
		});
	}
}

EOSIO_DISPATCH( token, (transfer) (foo))