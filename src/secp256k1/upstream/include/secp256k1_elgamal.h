//  Created by oc on 2019/7/26.
//  Copyright © 2019 oc. All rights reserved.
//

#ifndef secp256k1_elgamal_h
#define secp256k1_elgamal_h

# ifdef __cplusplus
extern "C" {
# endif

/* Encrypt data using Elgamal algorithm
 * Retuns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     out128: a 128-byte array which will be encrypted data (cannot be NULL), {PK^r, H^v * G^r}
 * In:      pubkey: the public encrypting key (cannot be NULL)
 *          random: the random number in G^r
 *          v: random value in H^v
 */
SECP256K1_API int secp256k1_elgamal_encrypt(
        const secp256k1_context *ctx,
        unsigned char *out128,
        const secp256k1_pubkey *pubkey,
        const unsigned char *random,
        unsigned int v
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4);

/* Decrypt data using Elgamal algorithm
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     v: a 64-bit array contains point, h^v
 * In:      in128: a 128-byte array encrypted data, {PK^r, H^v * G^r}
 *          prikey: private key
 */
SECP256K1_API int secp256k1_elgamal_decrypt(
        const secp256k1_context *ctx,
        unsigned char *v,
        const unsigned char *in128,
        const unsigned char *prikey
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4);

/* Find v by h^v, try from 0 to 1000, if v is bigger than 1000, should use other ways.
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     v: unsigned int value
 * In:      in64: a 64-byte array point, h^v
 */
SECP256K1_API int secp256k1_elgamal_find_v(
       const secp256k1_context *ctx,
       unsigned int *v,
       const unsigned char *in64
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3);

/* Decrypt two ciphertext using elgamal, E(pk, v1) + E(pk, v2) = E(pk, v1 + v2)
 * Args:    ctx: a secp256k1 context object
 * Out:     out128: a 128-bit array encrypted data
 * In:      in128_1: a 128-byte array encrypted data
 *          in128_2: a 128-byte array encrypted data
 */
SECP256K1_API int secp256k1_elgamal_add(
        const secp256k1_context *ctx,
        unsigned char *out128,
        const unsigned char *in128_1,
        const unsigned char *in128_2
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4);

/* Generate sender prove, the two ciphertext all used same public key encrypted
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     out160: a 160-byte array, 64 for A1 point, 64 for A2 point, 32 for Z number
 * In:      in128_1: first 128-byte array ciphertext
 *          in128_2: second 128-byte array ciphertext
 *          prikey: private key of public key that encrypted ciphertext
 *          random_a: a 32-byte array used to be a random number
 */
SECP256K1_API int secp256k1_elgamal_equal_prove(
        const secp256k1_context *ctx,
        unsigned char *out160,
        const unsigned char *in128_1,
        const unsigned char *in128_2,
        const unsigned char *prikey,
        const unsigned char *random_a
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4) SECP256K1_ARG_NONNULL(5) SECP256K1_ARG_NONNULL(6);

/* Verify sender prove
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * In:      prove160: a 160-byte array prove
 *          in128_1: first 128-byte array ciphertext
 *          in128_2: second 128-byte array ciphertext
 *          pubkey: public key that encrypted ciphertext
 */
SECP256K1_API int secp256k1_elgamal_equal_verify(
        const secp256k1_context *ctx,
        const unsigned char *prove160,
        const unsigned char *in128_1,
        const unsigned char *in128_2,
        const secp256k1_pubkey *pubkey
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4) SECP256K1_ARG_NONNULL(5);

/* Generate receiver prove
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     out192: a 192-byte array, 64 for A1 point, 64 for A2 point, 32 for Z1, 32 for Z2
 * In:      pubkey: public key that encrypted ciphertext
 *          random_a: a 32-byte array used to be a random number
 *          random_b: a 32-byte array used to be a random number
 *          random: a 32-byte array that used to be a random number same as in ciphertext
 *          v: same as encrypt v
 */
SECP256K1_API int secp256k1_elgamal_valid_prove(
        const secp256k1_context *ctx,
        unsigned char *out192,
        const secp256k1_pubkey *pubkey,
        const unsigned char *random_a,
        const unsigned char *random_b,
        const unsigned char *random,
        unsigned int v
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4) SECP256K1_ARG_NONNULL(5) SECP256K1_ARG_NONNULL(6);

/* Verify receiver prove
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * In:      prove192: a 192-byte array prove
 *          in128: a 128-byte array ciphertext
 *          pubkey: public key that encrypted ciphertext
 */
SECP256K1_API int secp256k1_elgamal_valid_verify(
        const secp256k1_context *ctx,
        const unsigned char *prove192,
        const unsigned char *in128,
        const secp256k1_pubkey *pubkey
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4);

/* Calculate secret key use r1 - r2 - r3
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     seckey: secret key
 * In:      r1: a 32-byte array
 *          r2: a 32-byte array
 *          r3: a 32-byte array
 */
SECP256K1_API int secp256k1_elgamal_seckey(
       const secp256k1_context *ctx,
       unsigned char *seckey,
       const unsigned char *r1,
       const unsigned char * r2,
       const unsigned char *r3
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4) SECP256K1_ARG_NONNULL(5);

/* Calculate public key use y1 - y2 - y3
 * Returns: 1 for success, 0 for fail
 * Args:    ctx: a secp256k1 context object
 * Out:     pubkey: public key
 * In:      y1: a 64-byte array
 *          y2: a 64-byte array
 *          y3: a 64-byte array
 */
SECP256K1_API int secp256k1_elgamal_pubkey(
       const secp256k1_context *ctx,
       secp256k1_pubkey* pubkey,
       const unsigned char *y1,
       const unsigned char *y2,
       const unsigned char *y3
) SECP256K1_ARG_NONNULL(1) SECP256K1_ARG_NONNULL(2) SECP256K1_ARG_NONNULL(3) SECP256K1_ARG_NONNULL(4) SECP256K1_ARG_NONNULL(5);

# ifdef __cplusplus
}
# endif

#endif /* secp256k1_elgamal_h */
