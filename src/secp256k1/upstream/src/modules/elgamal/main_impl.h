//
//  main_impl.h
//  secp256k1-zkp-elgamal
//
//  Created by oc on 2019/7/26.
//  Copyright © 2019 oc. All rights reserved.
//

#ifndef main_impl_h
#define main_impl_h

static const secp256k1_gej secp256k1_gej_const_h = SECP256K1_GEJ_CONST(
    0x50929b74UL, 0xC1A04954UL, 0xB78B4B60UL, 0x35E97A5EUL,
    0x078A5A0FUL, 0x28EC96D5UL, 0x47BFEE9AUL, 0xCE803AC0UL,
    0x31D3C686UL, 0x3973926EUL, 0x049E637CUL, 0xB1B5F40AUL,
    0x36DAC28AUL, 0xF1766968UL, 0xC30C2313UL, 0xF3A38904UL
);

static const secp256k1_scalar secp256k1_scalar_const_zero = SECP256K1_SCALAR_CONST(0,0,0,0,0,0,0,0);

//  local usage
typedef struct {
    secp256k1_ge x;
    secp256k1_ge y;
} secp256k1_elgamal_ciphertext;

static void secp256k1_elgamal_ge_to_string(unsigned char *r, const secp256k1_ge *p) {
    secp256k1_ge_storage s;

    secp256k1_ge_to_storage(&s, p);
    memcpy(r, &s, sizeof(s));
}

static void secp256k1_elgamal_ge_from_string(secp256k1_ge *p, const unsigned char *r) {
    secp256k1_ge_storage s;

    memcpy(&s, r, sizeof(s));
    secp256k1_ge_from_storage(p, &s);
}

static void secp256k1_elgamal_ciphertext_to_storage(unsigned char *r, const secp256k1_elgamal_ciphertext *a) {
    secp256k1_elgamal_ge_to_string(&r[0], &a->x);
    secp256k1_elgamal_ge_to_string(&r[64], &a->y);
}

static void secp256k1_elgamal_ciphertext_from_storage(secp256k1_elgamal_ciphertext* r, const unsigned char *a) {
    secp256k1_elgamal_ge_from_string(&r->x, &a[0]);
    secp256k1_elgamal_ge_from_string(&r->y, &a[64]);
}

SECP256K1_INLINE static int secp256k1_ge_eq(const secp256k1_ge* a, const secp256k1_ge* b) {
    return (secp256k1_fe_equal(&a->x, &b->x)
        && secp256k1_fe_equal(&a->y, &b->y)
            && a->infinity == b->infinity);
}

SECP256K1_INLINE static void secp256k1_ge_sub(secp256k1_gej *out, const secp256k1_ge *a, const secp256k1_ge *b) {
    // out = a / b
    secp256k1_gej aj, bj, neg_bj;
    secp256k1_ge neg_b;

    secp256k1_gej_set_ge(&aj, a);
    secp256k1_gej_set_ge(&bj, b);

    secp256k1_gej_neg(&neg_bj, &bj);
    secp256k1_ge_set_gej(&neg_b, &neg_bj);
    secp256k1_gej_add_ge(out, &aj, &neg_b);
}

SECP256K1_INLINE static void secp256k1_ge_ecmult_add(const secp256k1_context* ctx, secp256k1_gej *out, const secp256k1_ge *a, const secp256k1_gej *b, const secp256k1_scalar *e) {
    // out = a * b ^ e
    secp256k1_gej bej, aj;
    secp256k1_ge be;

    secp256k1_ecmult(&ctx->ecmult_ctx, &bej, b, e, &secp256k1_scalar_const_zero);
    secp256k1_ge_set_gej(&be, &bej);

    secp256k1_gej_set_ge(&aj, a);
    secp256k1_gej_add_ge(out, &aj, &be);
}

int secp256k1_elgamal_encrypt(const secp256k1_context* ctx, unsigned char *out128, const secp256k1_pubkey *pubkey, const unsigned char *random, unsigned int v) {
    secp256k1_ge pub, x, y;
    secp256k1_gej pubj, xj, yj;
    secp256k1_scalar r, sv;
    int ret = 0, overflow = 0;
    secp256k1_elgamal_ciphertext ct;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(out128 != NULL);
    ARG_CHECK(pubkey != NULL);
    ARG_CHECK(random != NULL);

    if (!secp256k1_pubkey_load(ctx, &pub, pubkey)) return ret;
    secp256k1_gej_set_ge(&pubj, &pub);

    secp256k1_scalar_set_b32(&r, random, &overflow);
    ret = (!overflow) & (!secp256k1_scalar_is_zero(&r));
    if (!ret) return ret;

    // x = PK ^ r
    secp256k1_ecmult(&ctx->ecmult_ctx, &xj, &pubj, &r, &secp256k1_scalar_const_zero);
    secp256k1_ge_set_gej(&x, &xj);

    secp256k1_scalar_set_int(&sv, v);

    // y = H ^ v * G ^ r
    secp256k1_ecmult(&ctx->ecmult_ctx, &yj, &secp256k1_gej_const_h, &sv, &r);
    secp256k1_ge_set_gej(&y, &yj);

    ct.x = x;
    ct.y = y;

    // convert ciphertext to storage
    secp256k1_elgamal_ciphertext_to_storage(out128, &ct);
    
    ret = 1;
    return ret;
}

int secp256k1_elgamal_decrypt(const secp256k1_context *ctx, unsigned char *v, const unsigned char *in128, const unsigned char* prikey) {
    int ret = 0, overflow = 0;
    secp256k1_elgamal_ciphertext ct;
    secp256k1_scalar sec, invs_sec;
    secp256k1_gej grj, xj, neg_grj, hvj;
    secp256k1_ge hv;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(v != NULL);
    ARG_CHECK(in128 != NULL);
    ARG_CHECK(prikey != NULL);

    // convert storage to ciphertext
    secp256k1_elgamal_ciphertext_from_storage(&ct, in128);

    // calculate prikey
    secp256k1_scalar_set_b32(&sec, prikey, &overflow);
    ret = (!overflow) & (!secp256k1_scalar_is_zero(&sec));
    if (!ret) return ret;

    // 1/sk
    secp256k1_scalar_inverse(&invs_sec, &sec);

    // x ^ 1/sk -> g^r
    secp256k1_gej_set_ge(&xj, &ct.x);
    secp256k1_ecmult(&ctx->ecmult_ctx, &grj, &xj, &invs_sec, &secp256k1_scalar_const_zero);

    // g^(-r)
    secp256k1_gej_neg(&neg_grj, &grj);

    // h^v
    secp256k1_gej_add_ge(&hvj, &neg_grj, &ct.y);
    secp256k1_ge_set_gej(&hv, &hvj);

    // save h^v to 64-byte array
    secp256k1_elgamal_ge_to_string(v, &hv);

    ret = 1;
    return ret;
}

int secp256k1_elgamal_find_v(const secp256k1_context *ctx, unsigned int *v, const unsigned char *in64) {
    secp256k1_scalar sv;
    secp256k1_gej xj;
    secp256k1_ge x, hv;
    int ret = 0;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(v != NULL);
    ARG_CHECK(in64 != NULL);

    secp256k1_elgamal_ge_from_string(&hv, in64);

    for (int i = 0; i < 1000; ++i) {    // we just try 1000 times
        secp256k1_scalar_set_int(&sv, i);
        secp256k1_ecmult(&ctx->ecmult_ctx, &xj, &secp256k1_gej_const_h, &sv, &secp256k1_scalar_const_zero);
        secp256k1_ge_set_gej(&x, &xj);
        if (secp256k1_ge_eq(&x, &hv)) {
            *v = i;
            ret = 1;
            break;
        }
    }

    return ret;
}

int secp256k1_elgamal_add(const secp256k1_context *ctx, unsigned char *out128, const unsigned char *in128_1, const unsigned char *in128_2) {
    secp256k1_elgamal_ciphertext ct;
    secp256k1_gej xj1, yj1, xj, yj;
    secp256k1_ge x, y;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(out128 != NULL);
    ARG_CHECK(in128_1 != NULL);
    ARG_CHECK(in128_2 != NULL);

    // convert storage 1 to ciphertext
    secp256k1_elgamal_ciphertext_from_storage(&ct, in128_1);

    // convert ge to gej
    secp256k1_gej_set_ge(&xj1, &ct.x);
    secp256k1_gej_set_ge(&yj1, &ct.y);

    // convert storage 2 to ciphertext
    secp256k1_elgamal_ciphertext_from_storage(&ct, in128_2);

    // add gej and ge
    secp256k1_gej_add_ge(&xj, &xj1, &ct.x);
    secp256k1_gej_add_ge(&yj, &yj1, &ct.y);

    // convert gej to ge
    secp256k1_ge_set_gej(&x, &xj);
    secp256k1_ge_set_gej(&y, &yj);

    ct.x = x;
    ct.y = y;

    // convert ciphertext to storage
    secp256k1_elgamal_ciphertext_to_storage(out128, &ct);

    return 1;
}

int secp256k1_elgamal_equal_prove(const secp256k1_context *ctx, unsigned char *out160, const unsigned char *in128_1, const unsigned char *in128_2, const unsigned char *prikey, const unsigned char *random_a) {
    int ret = 0, overflow = 0;
    secp256k1_elgamal_ciphertext ca1, ca2;
    secp256k1_ge a1, a2;
    secp256k1_gej xj, yj, a1j, a2j;
    secp256k1_scalar a, es, esk, z, sk;
    secp256k1_sha256 hasher;
    unsigned char tmp128[128] = { 0 };
    unsigned char e[32] = { 0 };

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(out160 != NULL);
    ARG_CHECK(in128_1 != NULL);
    ARG_CHECK(in128_2 != NULL);
    ARG_CHECK(prikey != NULL);
    ARG_CHECK(random_a != NULL);

    // convert storage to ciphertext
    secp256k1_elgamal_ciphertext_from_storage(&ca1, in128_1);
    secp256k1_elgamal_ciphertext_from_storage(&ca2, in128_2);

    // X = ca1.x / ca2.x, Y = ca1.y / ca2.y
    secp256k1_ge_sub(&xj, &ca1.x, &ca2.x);
    secp256k1_ge_sub(&yj, &ca1.y, &ca2.y);

    // convert a
    secp256k1_scalar_set_b32(&a, random_a, &overflow);

    // A1 = Y^a
    secp256k1_ecmult(&ctx->ecmult_ctx, &a1j, &yj, &a, &secp256k1_scalar_const_zero);

    // A2 = g^a
    secp256k1_ecmult_gen(&ctx->ecmult_gen_ctx, &a2j, &a);

    secp256k1_ge_set_gej(&a1, &a1j);
    secp256k1_ge_set_gej(&a2, &a2j);

    // stringify a1, a2
    secp256k1_elgamal_ge_to_string(&tmp128[0], &a1);
    secp256k1_elgamal_ge_to_string(&tmp128[64], &a2);

    // e = Hash(A1, A2)
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, tmp128, 128);
    secp256k1_sha256_finalize(&hasher, e);

    // Z = a + e*SK
    secp256k1_scalar_set_b32(&es, e, &overflow);
    secp256k1_scalar_set_b32(&sk, prikey, &overflow);
    secp256k1_scalar_mul(&esk, &es, &sk);
    secp256k1_scalar_add(&z, &a, &esk);

    // [A1, A2, Z]
    secp256k1_scalar_get_b32(e, &z);
    memcpy(out160, tmp128, 128);
    memcpy(&out160[128], e, 32);

    ret = 1;
    return ret;
}

int secp256k1_elgamal_equal_verify(const secp256k1_context *ctx, const unsigned char *prove160, const unsigned char *in128_1, const unsigned char *in128_2, const secp256k1_pubkey *pubkey) {
    int ret = 0, overflow = 0;
    secp256k1_ge left, right, pub, a1, a2;
    secp256k1_gej xj, yj, leftj, rightj, pubj;
    secp256k1_elgamal_ciphertext ca1, ca2;
    secp256k1_scalar z, es;
    secp256k1_sha256 hasher;
    unsigned char e[32] = { 0 };

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(prove160 != NULL);
    ARG_CHECK(in128_1 != NULL);
    ARG_CHECK(in128_2 != NULL);
    ARG_CHECK(pubkey != NULL);

    if (!secp256k1_pubkey_load(ctx, &pub, pubkey)) return ret;
    secp256k1_gej_set_ge(&pubj, &pub);

    // parse first 64 byte to A1, second 64 byte to A2, last 32 byte to Z.
    secp256k1_elgamal_ge_from_string(&a1, &prove160[0]);
    secp256k1_elgamal_ge_from_string(&a2, &prove160[64]);
    secp256k1_scalar_set_b32(&z, &prove160[128], &overflow);

    // convert storage to ciphertext
    secp256k1_elgamal_ciphertext_from_storage(&ca1, in128_1);
    secp256k1_elgamal_ciphertext_from_storage(&ca2, in128_2);

    // calculate e = Hash(A1, A2)
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, prove160, 128);
    secp256k1_sha256_finalize(&hasher, e);

    // X = ca1.x / ca2.x, Y = ca1.y / ca2.y
    secp256k1_ge_sub(&xj, &ca1.x, &ca2.x);
    secp256k1_ge_sub(&yj, &ca1.y, &ca2.y);

    // verify Y ^ Z = A1 * X ^ e
    // Y ^ Z
    secp256k1_ecmult(&ctx->ecmult_ctx, &leftj, &yj, &z, &secp256k1_scalar_const_zero);

    // A1 * X ^ e
    secp256k1_scalar_set_b32(&es, e, &overflow);
    secp256k1_ge_ecmult_add(ctx, &rightj, &a1, &xj, &es);

    secp256k1_ge_set_gej(&left, &leftj);
    secp256k1_ge_set_gej(&right, &rightj);

    ret = secp256k1_ge_eq(&left, &right);
    if (!ret) return ret;

    // verify g ^ Z = A2 * PA ^ e
    secp256k1_ecmult_gen(&ctx->ecmult_gen_ctx, &leftj, &z);

    // A2 * PA ^ e
    secp256k1_ge_ecmult_add(ctx, &rightj, &a2, &pubj, &es);

    secp256k1_ge_set_gej(&left, &leftj);
    secp256k1_ge_set_gej(&right, &rightj);

    ret = secp256k1_ge_eq(&left, &right);
    return ret;
}

int secp256k1_elgamal_valid_prove(const secp256k1_context *ctx, unsigned char *out192, const secp256k1_pubkey *pubkey, const unsigned char *random_a, const unsigned char *random_b, const unsigned char *random, unsigned int v) {
    int ret = 0, overflow = 0;
    secp256k1_scalar a, b, es, r, re, z1, z2, vs, ve;
    secp256k1_ge pub, a1, a2;
    secp256k1_gej pubj, a1j, a2j;
    unsigned char tmp128[128] = { 0 };
    unsigned char e[32] = { 0 };
    secp256k1_sha256 hasher;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(out192 != NULL);
    ARG_CHECK(pubkey != NULL);
    ARG_CHECK(random != NULL);

    if (!secp256k1_pubkey_load(ctx, &pub, pubkey)) return ret;
    secp256k1_gej_set_ge(&pubj, &pub);

    secp256k1_scalar_set_b32(&a, random_a, &overflow);
    secp256k1_scalar_set_b32(&b, random_b, &overflow);
    secp256k1_scalar_set_b32(&r, random, &overflow);
    secp256k1_scalar_set_int(&vs, v);

    // A1 = PB ^ a
    secp256k1_ecmult(&ctx->ecmult_ctx, &a1j, &pubj, &a, &secp256k1_scalar_const_zero);

    // A2 = g ^ a * h ^ b
    secp256k1_ecmult(&ctx->ecmult_ctx, &a2j, &secp256k1_gej_const_h, &b, &a);

    secp256k1_ge_set_gej(&a1, &a1j);
    secp256k1_ge_set_gej(&a2, &a2j);

    secp256k1_elgamal_ge_to_string(&tmp128[0], &a1);
    secp256k1_elgamal_ge_to_string(&tmp128[64], &a2);

    // e = Hash(A1, A2)
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, tmp128, 128);
    secp256k1_sha256_finalize(&hasher, e);

    // Z1 = a + r * e, Z2 = b + v * e
    secp256k1_scalar_set_b32(&es, e, &overflow);
    secp256k1_scalar_mul(&re, &es, &r);
    secp256k1_scalar_add(&z1, &a, &re);

    secp256k1_scalar_mul(&ve, &es, &vs);
    secp256k1_scalar_add(&z2, &b, &ve);

    // [A1, A2, Z1, Z2]
    memcpy(out192, tmp128, 128);

    secp256k1_scalar_get_b32(e, &z1);
    memcpy(&out192[128], e, 32);

    secp256k1_scalar_get_b32(e, &z2);
    memcpy(&out192[160], e, 32);

    ret = 1;
    return ret;
}

int secp256k1_elgamal_valid_verify(const secp256k1_context *ctx, const unsigned char *prove192, const unsigned char *in128, const secp256k1_pubkey *pubkey) {
    int ret = 0, overflow = 0;
    secp256k1_elgamal_ciphertext cb1;
    secp256k1_scalar z1, z2, es;
    secp256k1_gej pubj, leftj, rightj, xj, yj;
    secp256k1_ge pub, left, right, a1, a2;
    unsigned char e[32] = { 0 };
    secp256k1_sha256 hasher;

    ARG_CHECK(ctx != NULL);
    ARG_CHECK(prove192 != NULL);
    ARG_CHECK(in128 != NULL);
    ARG_CHECK(pubkey != NULL);

    if (!secp256k1_pubkey_load(ctx, &pub, pubkey)) return ret;
    secp256k1_gej_set_ge(&pubj, &pub);

    // parse first 64 byte to A1, second 64 byte to A2, third 32 byte to Z1, last 32 byte to Z2
    secp256k1_elgamal_ge_from_string(&a1, &prove192[0]);
    secp256k1_elgamal_ge_from_string(&a2, &prove192[64]);
    secp256k1_scalar_set_b32(&z1, &prove192[128], &overflow);
    secp256k1_scalar_set_b32(&z2, &prove192[160], &overflow);

    // parse to CB1
    secp256k1_elgamal_ciphertext_from_storage(&cb1, in128);

    // calculate e = Hash(A1, A2)
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, &prove192[0], 128);
    secp256k1_sha256_finalize(&hasher, e);

    secp256k1_gej_set_ge(&xj, &cb1.x);
    secp256k1_gej_set_ge(&yj, &cb1.y);

    // verify PK ^ Z1 = A1 * X ^ e
    secp256k1_ecmult(&ctx->ecmult_ctx, &leftj, &pubj, &z1, &secp256k1_scalar_const_zero);

    secp256k1_scalar_set_b32(&es, e, &overflow);
    secp256k1_ge_ecmult_add(ctx, &rightj, &a1, &xj, &es);

    secp256k1_ge_set_gej(&left, &leftj);
    secp256k1_ge_set_gej(&right, &rightj);

    ret = secp256k1_ge_eq(&left, &right);
    if (!ret) return ret;

    // verify g ^ Z1 * h ^ Z2 = A2 * Y ^ e
    secp256k1_ecmult(&ctx->ecmult_ctx, &leftj, &secp256k1_gej_const_h, &z2, &z1);
    secp256k1_ge_ecmult_add(ctx, &rightj, &a2, &yj, &es);

    secp256k1_ge_set_gej(&left, &leftj);
    secp256k1_ge_set_gej(&right, &rightj);

    ret = secp256k1_ge_eq(&left, &right);
    return ret;
}

int secp256k1_elgamal_seckey(const secp256k1_context *ctx, unsigned char *seckey, const unsigned char *r1, const unsigned char * r2, const unsigned char *r3)
{
    ARG_CHECK(ctx != NULL);
    ARG_CHECK(seckey != NULL);
    ARG_CHECK(r1 != NULL);
    ARG_CHECK(r2 != NULL);
    ARG_CHECK(r3 != NULL);

    secp256k1_scalar s1, s2, s3, tmp, s2_neg, s3_neg, fin;
    int overflow = 0;
    secp256k1_scalar_set_b32(&s1, r1, &overflow);
    secp256k1_scalar_set_b32(&s2, r2, &overflow);
    secp256k1_scalar_set_b32(&s3, r3, &overflow);

    secp256k1_scalar_negate(&s2_neg, &s2);
    secp256k1_scalar_add(&tmp, &s1, &s2_neg);

    secp256k1_scalar_negate(&s3_neg, &s3);
    secp256k1_scalar_add(&fin, &tmp, &s3_neg);

    secp256k1_scalar_get_b32(seckey, &fin);

    return 1;
}

int secp256k1_elgamal_pubkey(const secp256k1_context *ctx, secp256k1_pubkey* pubkey, const unsigned char *y1, const unsigned char *y2, const unsigned char *y3)
{
    ARG_CHECK(ctx != NULL);
    ARG_CHECK(pubkey != NULL);
    ARG_CHECK(y1 != NULL);
    ARG_CHECK(y2 != NULL);
    ARG_CHECK(y3 != NULL);

    secp256k1_ge g1, g2, g3, temp, fin;
    secp256k1_gej tempj, finj;
    secp256k1_elgamal_ge_from_string(&g1, y1);
    secp256k1_elgamal_ge_from_string(&g2, y2);
    secp256k1_elgamal_ge_from_string(&g3, y3);

    secp256k1_ge_sub(&tempj, &g1, &g2);
    secp256k1_ge_set_gej(&temp, &tempj);

    secp256k1_ge_sub(&finj, &temp, &g3);
    secp256k1_ge_set_gej(&fin, &finj);

    secp256k1_pubkey_save(pubkey, &fin);

    return 1;
}
#endif /* main_impl_h */
