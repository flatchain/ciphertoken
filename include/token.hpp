#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>

using namespace eosio;
using namespace std;

CONTRACT token : public contract {
   public:
      using contract::contract;

      ACTION transfer(name from, name to, /*symbol symbol, */string fpubkey, string tpubkey, string eq_para, string from_pub_para, string to_pub_para, string signature, string memo);
      ACTION foo();

      struct [[eosio::table]] account {
         name     id;
         string   cipher;

         uint64_t primary_key()const { return id.value; }
      };

      typedef eosio::multi_index< "accounts"_n, account > accounts;

      using transfer_action = action_wrapper<"transfer"_n, &token::transfer>;
};